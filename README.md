# README #

## MTR interface with GO ##

### What is this repository for? ###

* To build a network monitoring tool which can give insights into network latency 
* Intergrating MTR with Prometheus via OpenTelemetry
* Version: 0.4a

#### 0.4a ####
* version 0.22a in binary in icmp_t (will be depricated)
* version 0.3a is also included but not complied
* Fixed IP Addresses incase of no success
* Implemented more efficent form of for loop (packet per IP)
* Fixed Failed attempts no more retransmission if retransmission should be re enabled we must fix the sequence numbers otherwise error in time values 
* Created Delays per hop and per packet inorder to prevent spamming of packets and DDOS 

### How do I get set up? ###
**from source**

``` 
go build <file_name.go>
sudo ./<file_name>
```
---

**from premade binary**
```
cd <git cloned folder>
sudo ./bin/icmp
```
**Note udp4 does not work in go when reciving packets (ttl exceed not captured) hence we are forced to use icmp and which implies sudo is required**

#### Useful links ####
* The [net package](https://godoc.org/net)
* The [ipv4 package](https://godoc.org/golang.org/x/net/ipv4)
* The [icmp package](https://godoc.org/golang.org/x/net/icmp)
* [IANA proto numbers](https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml)
* [echo source code](https://github.com/golang/net/blob/master/icmp/echo.go)

### Who do I talk to? ###

* Anyone in the members section

### To be Implemented ###
#### 0.1a ####
1. Proper traceroute with 3 test per IP
1. Combination with ping
1. Interface via OpenTelemetry

#### 0.2a ####
1. More details Like loss, min,avg
1. Needs Live update if route is changed instead of static 
1. More packets like UDP,TCP, etc (currently supports only ICMP)
1. max hops by the user, target destination, timeout 

#### 0.3a ####
1. Needs ID Match fix **ASAP** but trials with opentelemtry can be done in parallel 
1. prometheus via opentelemtry needs to be implement 
1. grafana implementation
1. More packets like UDP,TCP, etc (currently supports only ICMP) (Not Important)
1. max hops by the user, target destination, timeout(Not Important)

#### 0.4a ####
1. mtr via prometheus 
1. udp support
1. maybe jitter calculation and Autonomous System lookup needed
1. Trace section of the code has to be optimized cause any program can throttle the read dead line and it can take an avg of 30 secs to complete 


### Sample Output ###
**Please not that this is done under a vpn connection hence high ping**
---
**0.1a**

![Alt text](extras/firstrun.png) 
---
**0.2a**
![Alt text](extras/mtr0.2a.png)
---
**0.3a**
![Alt text](extras/mtr0.3a.png)
---
**0.4a**
![Alt text](extras/mtr0.4a.png)