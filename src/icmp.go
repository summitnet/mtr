package main

import (
    "log"
    "golang.org/x/net/icmp"
    "golang.org/x/net/ipv4"
    "os"
    "time"
    "fmt"
    "net"
    "encoding/binary"
)

var mypid int=os.Getpid() & 0xffff;
var pingperroute=5;

func rawpas(mainbody []byte)(identity,sequence uint16){
    identity=binary.BigEndian.Uint16(mainbody[28:30])
    sequence=binary.BigEndian.Uint16(mainbody[30:32])
    return
}



func err_hdlr(err error){
    if(err!=nil){
        log.Fatal(err);
    }
    
}

func alyz(t []time.Duration) (avg,min,max time.Duration){//analyze function
    min=time.Duration(9999*time.Millisecond)
    max=time.Duration(-1*time.Millisecond)
 //   fmt.Println(t)
    var e int64
    for _,i:=range t{
        //fmt.Println(i.Nanoseconds(),min.Nanoseconds(),max.Nanoseconds())
        if(i.Microseconds()<min.Microseconds()){
            min=i
        }
        if(i.Microseconds()>max.Microseconds()){
            max=i
        }
        e+=i.Microseconds();
    }
    size:=int64(len(t))
    avg=time.Duration(e/size)*time.Microsecond
    return
}

func sum(a []int) float32{
    var count float32;
    for _,num:=range a{
        count+=float32(num);
    }
    size:=len(a)
    return (count/float32(size))*100;
}
//website net.UDPAddr
func trace(i int,wm icmp.Message,p *icmp.PacketConn,website net.IPAddr)(rm *icmp.Message,peerstr string,rtt time.Duration){
    time.Sleep(50*time.Millisecond)
    var failed_attempt int;    
    wm.Body.(*icmp.Echo).Seq=i
    wb,err:=wm.Marshal(nil)
    err_hdlr(err);
    rb:=make([]byte,1500)
    begin:=time.Now();
    _,err=p.WriteTo(wb,&website);
    err_hdlr(err)
    for ;failed_attempt<5; {
        err=p.SetReadDeadline(time.Now().Add(2*time.Second))
        err_hdlr(err)
        n,peer,err:=p.ReadFrom(rb);
        
        if err!=nil{
            if err,ok:=err.(net.Error);ok && err.Timeout(){
                
                return nil, "", 0*time.Second
            }else{
                err_hdlr(err)
            }
            
        }
        peerstr=peer.String()
        rm,err=icmp.ParseMessage(1,rb[:n])//test no n
        err_hdlr(err)
        //fmt.Println(rm.Body.Marshal(1))
        rtt=time.Since(begin)
        switch body := rm.Body.(type) {
            case *icmp.Echo:
                if(mypid==body.ID && i==body.Seq){
                    return
                }else{
                    failed_attempt++;
                }
            default:
                bod_extract,err:=rm.Body.Marshal(1)
                err_hdlr(err)
                iden,seqnc:=rawpas(bod_extract)
                if(mypid==int(iden) && i==int(seqnc)){
                    return
                }else{
                    failed_attempt++;
                }
        }
    }
    return nil, "", 0*time.Second
}

func main(){
    //c, err := icmp.ListenPacket("udp4", "0.0.0.0")
    c, err := icmp.ListenPacket("ip4:icmp", "0.0.0.0")
    if err != nil {
        log.Fatal("here",err)
    }
    defer c.Close()
    if(err!=nil){
        log.Fatal(err,"here2")
    }
     wm := icmp.Message{
         Type: ipv4.ICMPTypeEcho, Code: 0,
         Body: &icmp.Echo{
             ID: mypid, Seq: 1,
             Data: []byte("01213456789ABCDEF"),
         },
     }
     
    //fmt.Println("id\t\tIP\t\tTime\t\tTime2\t\tTime3")
    fmt.Printf("%-10v%-26v%-26v%-26v%-26v%v\n","ID","IP","avg","min","max","Loss")
    var j=1;

    google:=net.IPAddr{
        IP: net.IP{8,8,8,8},
    }

            
    
    for i:=1;i<31;i++ {
        time.Sleep(100*time.Millisecond)
        var count=make([]int,pingperroute);
        var times=make([]time.Duration,pingperroute)
        var peer string="*";
        var index int;
        err = c.IPv4PacketConn().SetTTL(i);
        err_hdlr(err);
        // google:=net.UDPAddr{
        var rm *icmp.Message;
        var rtt time.Duration;
        var peertmp string;
        for k:=j+pingperroute;j<k;j++{
            rm, peertmp, rtt = trace(j, wm, c, google)
            if(peertmp!="" && peer=="*"){
                peer=peertmp;
            }
            if (rm == nil) {
                //fmt.Printf("%-10v%-26v%-26v", i,"*","*")
                times[index] = -1 * time.Microsecond
                count[index] = 1
            }else {
                if (rm.Type == ipv4.ICMPTypeTimeExceeded || rm.Type == ipv4.ICMPTypeEchoReply) {
                    times[index] = rtt
                    //fmt.Printf("%-10v%-26v%-26v",i,peer,rtt)
                } else {
                    //fmt.Printf("%-10v%-26v%-26v",i,"failed","failed")
                    times[index] = -1 * time.Microsecond
                }
            }
            index++;
            
        }

        avg,min,max:=alyz(times)
        fmt.Printf("%-10v%-26v%-26v%-26v%-26v%v%%\n",i,peer,avg,min,max,sum(count))
       
        if(rm!=nil){//can't access type if rm=nil
            if(rm.Type==ipv4.ICMPTypeEchoReply){
                break
            }
        }
    }

}
// reference to binary parising https://github.com/golang/net/blob/master/icmp/echo.go